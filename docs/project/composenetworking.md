# Сети в Docker Compose

## Сеть по умолчанию

По умолчанию Compose создает единую сеть для приложения. Каждый контейнер сервиса подключается к сети по-умолчанию и доступен другим контейнерам в этой сети, имея хостнейм идентичный имени контейнера.

Сеть приложения имеет имя по названию директории в которой находится файл описания Compose, однако его можно задать флагом `--project-name` или переменной окружения `COMPOSE_PROJECT_NAME`.

В качестве примера представим, что приложение находится в директории `myapp`, а `docker-compose.yml` выглядит следующим образом:

```
version: "3"
services:
  web:
    build: .
    ports:
      - "8000:8000"
  db:
    image: postgres
    ports:
      - "8001:5432"
```

 После выполнения `docker-compose up` произойдет следующее:

1. Создается сеть `myapp_default`;
2. Контейнер, использующий конфигурацию `web` имеет DNS имя `web` внутри сети `myapp_default`;
3. Контейнер, использующий конфигурацию `db` имеет DNS имя `db` внутри сети `myapp_default`.

Надо так же учитывать, что начиная с версии 2.1 Compose файла `overlay` сети по умолчанию имеют параметр `attachable` и свойство `attachable` можно задать на `false`, начиная с версии 3.0 Compose файла.

При изменении параметров контейнера в Docker Compose файлах, обновить параметры приложения можно выполнив `docker-compose up`. Контейнеры будут пересозданы с другими IP адресами, если они не заданы статически в пользовательских сетях для контейнеров.

## Настройка сети по-умолчанию

Вместо описания своих сетей можно настроить стандартную сеть для контейнера. Для этого необходимо изменять запись `default` внутри записи `networks`.

```
version: "3"
services:

  web:
    build: .
    ports:
      - "8000:8000"
  db:
    image: postgres

networks:
  default:
    # Use a custom driver
    driver: overlay
    attachable: true
    ipam:
      driver: default
      config:
        - subnet: 172.28.0.0/16
```

## Использование существующей сети

Если сеть уже создана (например, через Docker CLI), то подключиться к ней можно с использованием опции `external`:

```
networks:
  default:
    external:
      name: my-pre-existing-network
```

Таким образом вместо создания сети `[projectname]_default`, Compose подключит приложение к сети `my-pre-existing-network`.

## Создание сети

Конечно же можно создать свою сеть в Docker Compose. Вначале приведем `docker-compose.yml` для представление как можно описать сети и поясним что происходит:

```
version: '2.3'
#network_mode: "bridge"
services:
  nginx:
    image: nginx
    ports:
      - "80:80"
    networks:
      back:
        ipv4_address: 172.16.238.2
        ipv6_address: 2001:3984:3989::2
      default:

  wetty:
    build: 
      context: ./wetty
      dockerfile: Dockerfilealt
    image: wetty:latest
    ports:
      - "3000:3000"
    networks:
      back:
        ipv4_address: 172.16.238.3 
        ipv6_address: 2001:3984:3989::3 

  codiad:
    build: ./docker-codiad
    image: docker-codiad:latest
    volumes:
       - ./code:/code
       - /etc/localtime:/etc/localtime:ro
    ports:
      - "5000:5000"
      - "8080:8080"
    networks:
      back:
        ipv4_address: 172.16.238.4
        ipv6_address: 2001:3984:3989::4
      default:

networks:
#Internal-only network for proper nginx proxying and ssh
  back:
    driver: bridge
    enable_ipv6: true
    internal: true
    ipam:
     driver: default
     config:
       - subnet: 172.16.238.0/24
       - subnet: 2001:3984:3989::/64 
#External network actually
  default:
    driver: bridge
```

В файле описывается внутренняя `bridge` сеть с своей подсетью (`back`), а так же внешняя сеть, основанная на стандартной (`default`). У некоторых контейнеров заданы статические IP-адреса.

Необходимо отметить, что в Dockerfile 2 версии в целом больше настраиваемых параметров вроде IPv6 адресации, а в Dockerfile 3 версии таких параметров нет, так как их пока не поддерживает Docker Swarm (сделано было для удобства).

### Наглядная иерархия параметров в Docker Compose

Стоит учитывать, что многие параметры в представленном ниже описании конфликтуют, но они были описаны вместе для удоства и понимания иерархии параметров.

```
networks:
  # Имя default для реконфигурации стандартной сети для Compose
  имясети:
    # Типы драйверов: bridge, overlay  
    driver: overlay
    # attachable работает только в Compose 3.2+
    attachable: true
    # параметры драйвера (см. документацию драйвера)
    driver_opts:
      foo: "bar"
      baz: 1
    # Поддержка IPv6 не доступна в Compose 3, только 2.
    enable_ipv6: true
    # Настройка IPAM конфигурации
    ipam:
      # драйвер IPAM заместо default если нужно
      driver: default
      # Конфигурация с 0 и больше блоков, содержащая ключ subnet:
      config:
        - subnet: 172.28.0.0/16
        - subnet: 192.168.10.0/24
    
    # Делает сеть изолированной
    internal: true
    
    # Использование сети, созданной вне Compose.
    # Если параметр external имеет флаг true, то это указывает Compose, что сеть была создана извне. docker-compose up ее не создаст и если не найдет, то выкинет ошибку.
    # external не может быть использован с другими ключами конфигурации (driver, driver_opts, ipam, internal)
    external: true
    
    # Для external можно задать имя для использования в Compose файле начиная с версии 3.5:
    external:
      name: actual-name-of-network
      
    # Так же с версии 3.5 можно задать имя сети даже с спецсимволами:
  network1:
    # Использование файла external так же работает в купе с заданным именем сети
    external: true
    name: my-app-net
    
    # Метаданные для контейнера могут быть созданы с помощью Docker labels: это массив или словарь.
    labels:
      com.example.description: "Financial transaction network"
      com.example.department: "Finance"
      com.example.label-with-empty-value: ""
```

Версия без комментариев:

```
networks:
  networkname:
    driver: overlay
    attachable: true
    driver_opts:
      foo: "bar"
      baz: 1
    enable_ipv6: true
    ipam:
      config:
        - subnet: 172.28.0.0/16
        - subnet: 192.168.10.0/24
    internal: true
    external: true
    
    external:
      name: actual-name-of-network
      
  network1:
    external: true
    name: my-app-net
    labels:
      com.example.description: "Financial transaction network"
      com.example.department: "Finance"
      com.example.label-with-empty-value: ""
```



### Описание host и none режимов сети

`host` и `none ` режимы отличаются в описании по сравнению с `bridge` и `overlay`. Для них и не требуются какие-либо еще параметры.

```
# Host
services:
  web:
    ...
    networks:
      hostnet: {}

networks:
  hostnet:
    external:
      name: host
```

```
# None
services:
  web:
    ...
    networks:
      nonet: {}

networks:
  nonet:
    external:
      name: none
```

